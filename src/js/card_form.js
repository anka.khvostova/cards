import { createVisit } from './modal_login.js';
import { Modal } from './class_modal.js';
import { FormSelect } from './classes_form.js';
import { VisitCardiologist, VisitDentist, VisitTherapist } from './class_visit.js';
const cardBox = document.querySelector('.cards-box');






createVisit.addEventListener('click', () => {
    const popup = new Modal();
    popup.renderModal(createVisit, modalWindow);

    // =======================Создаём форму для карточки====================================

    const selectDoctor = new FormSelect(popup.modal);
    selectDoctor.renderFormSelect(doctorSelect, selectComponents);

    // -------Добавляем обработчик на селект для врача-----------------------------------------------------------------------------------


    selectDoctor.select.addEventListener('change', function () {
        const form = popup.modal.querySelector('form');
        if (form) {
            form.remove();
        }

        let visit;
        const target = this.options[this.selectedIndex].value;
        if (target === 'cardio') {
            this.doctor = target;
            visit = new VisitCardiologist({});
            // console.log(visit)

            visit.renderVisitForm(popup.modal, sendCardInfo, stylesForm, inputsForCard);
        }
        else if (target === 'dentist') {
            this.doctor = target;
            visit = new VisitDentist({});

            visit.renderVisitForm(popup.modal, sendCardInfo, stylesForm, inputsForCard);

        }
        else if (target === 'terapevt') {
            this.doctor = target;
            visit = new VisitTherapist({});

            visit.renderVisitForm(popup.modal, sendCardInfo, stylesForm, inputsForCard);

        }

    })
});



// -------------------==== Selects === ------------------------------------------------------------
const doctorSelect = {
    atributes: {
        id: 'doctorSelect',
        required: 'true',
        name: 'doctor_card',
        form: 'formCardCreate'
    },
    className: 'form-input',

}
const selectComponents = {
    classNameLabel: "label",
    labelText: "Выбрать врача",
    options: [['no_doctor', "не выбрано"], ['cardio', "Кардиолог"], ['terapevt', "Терапевт"], ['dentist', "Стоматолог"]],
};
// --------------====== Inputs =====------------------------------------------------
const inputsForCard = {
    form: {
        atributes: {
            id: 'formCardCreate',
            name: 'form-card',
        },
        className: '',
    },
    prioritySelect: {
        atributes: {
            id: 'ugrentlySelect',
            required: 'true',
            name: 'doctor_card',
            form: 'formCardCreate',
        },
        className: 'form-input',
    },

selectPriority: {
    classNameLabel: "label",
        labelText: "Срочность",
            options: [['low', "Обычная", 'true'], ['normal', "Приоритетная"], ['high', "Неотложная"]],
    },
// ------------Input for card--------------------------------------------------------------------------------

inputName: {
    atributes: {
        type: 'text',
            required: 'true',
                placeholder: 'ФИО',
                    name: 'patient_name',
        },
    className: 'form-input',
    },

inputVisitPurpose: {
    atributes: {
        type: 'text',
            required: 'true',
                placeholder: 'Цель визита',
                    name: 'visit_purpose',
        },
    className: 'form-input',
    },

inputVisitAge: {
    atributes: {
        type: 'number',
            required: 'true',
                placeholder: 'Возраст',
                    name: 'age',

        },
    className: 'form-input',
    },

inputVisitPressure: {
    atributes: {
        type: 'number',
            required: 'true',
                placeholder: 'Кровеносное давление',
                    name: 'blood_pressure',
        },
    className: 'form-input',

    },
inputVisitMass: {
    atributes: {
        type: 'number',
            required: 'true',
                placeholder: 'Индекс массы тела',
                    name: 'index_mass',
        },
    className: 'form-input',
    },
inputVisitLast: {
    atributes: {
        type: 'date',
            required: 'true',
                placeholder: 'Дата последнего визита',
                    name: 'last-visit',
        },
    className: 'form-input',
    },
submitFormBtn: {
    atributes: {

        type: 'submit',
            id: 'create_form',
                value: 'Создать карточку',
                    name: 'submit_btn',
        },
    className: 'form-btn',

    },
resetFormBtn: {
    atributes: {
        type: 'reset',
            id: 'reset_form',
                value: 'Очистить карточку',
                    name: 'reset_btn',
        },
    className: 'form-btn',
    },
textareaDescription: {
    atributes: {
        id: 'description',
            name: 'description',
                placeholder: 'Краткое описание',
                    // required: 'true',
                    rows: 5,
        },
    className: 'form-input',
    },
textareaIllnesses: {
    atributes: {
        id: 'illnesses',
            name: 'illnesses',
                placeholder: 'Ранее перенесенные заболевания',
                    // required: 'true',
                    rows: 5,
        },
    className: 'form-input',
    },
}
// --------------------- Styles ----------------------------------
// ------------Classes styles----------------------------------------------------------
export const stylesForm = {
    className: 'form-input',
    btnFormClass: 'form-btn',
    formClass: 'cards-create-form',
};
const sendCardInfo = {
    url: 'https://ajax.test-danit.com/api/v2/cards',
    method: 'POST',
    sessionStorKey: 'newCard',
};
// --------------Modal styles -------------------------------------
export const modalWindow = {
    modalClass: 'card-modal',
    btnClass: 'login-modal-btn',
    top: '25%',
    left: '30%',
    right: '30%',
}