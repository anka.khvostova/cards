import { cardToBoard } from './class_card_to_board.js';



export async function renderCardsToBoard(notification, container, { url, styles }) {
    await fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${sessionStorage.getItem('token')}`
        },

    }).then(response => response.json())
        .then((cardsList) => {
            console.log(cardsList);
            if (cardsList.length == 0) {
               console.log('No Items');
            }
            else {
                for (let card of cardsList) {
                    notification.style.display = 'none';
                    console.log(styles);
                    const boardCard = new cardToBoard(card, container);
                    boardCard.renderToBoard(styles);

                }

            }
        })
} 