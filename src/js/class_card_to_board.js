export class cardToBoard {
    constructor(visit, container) {
        this.container = container;
        this.card = document.createElement('div');
        this.visit = visit;

    }
    renderToBoard(styles) {
        this.createCard(styles);
        this.showMore();

    }
    createCard({ formClass, inputClassName, btnFormClass }) {

        this.style = inputClassName;
        this.btnStyle = btnFormClass;
        this.styleCotainer = formClass;
        this.card.classList.add(this.styleCotainer);
        this.name = document.createElement('p');
        this.doctor = document.createElement('p');
        this.name.classList.add(this.style);
        this.doctor.classList.add(this.style);
        this.name.textContent = 'Ф.И.О : ' + this.visit.fullname;
        this.doctor.textContent = this.visit.title;
        this.btn = document.createElement('button');
        this.btn.classList.add(this.btnStyle);
        this.btn.innerText = 'Показать больше';
        this.container.append(this.card);
        this.card.append(this.name, this.doctor, this.btn);
        // -------------------------------------------
        console.log(this.visit);
    }

    showMore() {
        this.btn.addEventListener('click', () => {
            this.container2 = document.createElement('div');
            this.container2.classList.add(this.styleCotainer);
            for (let key in this.visit) {
                if (key === 'fullname' || key === 'title' || key === 'form' || key === 'doctor') {
                    console.log(key);
                }
                else {
                    let info;
                    switch (key) {
                        case 'visitPurpose':
                            info = 'Цель визита: ';
                            break;
                        case 'age':
                            info = 'Возраст: ';
                            break;
                        case 'ugrency':
                            info = 'Срочность: ';
                            break;
                        case 'description':
                            info = 'Описание: ';
                            break;
                        case 'ugrency':
                            info = 'Срочность: ';
                            break;
                        case 'id':
                            info = 'Номер карточки: ';
                            break;
                        case 'lastVisitData':
                            info = 'Дата последнего визита: ';
                            break;
                        case 'id':
                            info = 'Вес: ';
                            break;
                        case 'pressure':
                            info = 'Давление: ';
                            break;
                        case 'bodyMassIndex':
                            info = 'pastIllnesses: ';
                            break;
                    }

                    let p = document.createElement('p');
                    p.classList.add(this.style);
                    p.innerHTML = info +
                        + this.visit[key];
                    this.container2.append(p);
                }
            }
            this.btn.style.display = 'none';
            this.rollUp = document.createElement('button');
            this.rollUp.textContent = 'Свернуть';
            this.edit = document.createElement('button');
            this.edit.classList.add(this.btnStyle);
            this.edit.textContent = "Изменить";
            // this.del.classList.add(btnFormClass);
            // this.edit.classList.add(btnFormClass);
            this.container2.append(this.rollUp, this.edit);
            this.card.append(this.container2);
            this.editCard();
            this.changerollUp();

        })
    }
    changerollUp() {
        this.rollUp.addEventListener('click', () => {
            this.btn.style.display = 'inline';
            this.container2.remove();

            this.rollUp.remove();
        });
    }
    editCard() {
        this.edit.addEventListener('click', () => {
            this.del = document.createElement('button');
            this.edit2 = document.createElement('button');
            this.edit2.classList.add(this.btnStyle);
            this.del.classList.add(this.btnStyle);
            this.del.textContent = "Удалить карточку";
            this.edit2.textContent = "Редактировать";
            this.container2.append(this.del, this.edit2);
            this.edit.remove();
            this.delCard()
        })

    }


    delCard() {
        const token = sessionStorage.getItem('token');
        this.del.addEventListener('click', () => {
            fetch("https://ajax.test-danit.com/api/v2/cards/" + this.visit.id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
            })
                .then(resp => resp.text())
                .then((resp) =>
                    console.log('удалено'),
                    this.card.remove())
        })

    }
}