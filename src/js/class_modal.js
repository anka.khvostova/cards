export class Modal {
    constructor() {
        this.modal = document.createElement('div');
        this.closeBtn = document.createElement('button');
    }

    createModalBlock() {
        this.closeBtn.innerText = 'X';
        this.modal.append(this.closeBtn);
        this.modal.style.position = 'fixed';
        this.modal.style.top = '0%';
        this.modal.style.left = '0%';
        document.body.append(this.modal);
    }
    addStylesToModal({modalClass, btnClass,  top = '5%', right = 'auto', bottom = 'auto', left = 'auto' }) {
        this.modal.classList.add(modalClass);
        this.closeBtn.classList.add(btnClass);
        
        this.modal.style.top = top;
        this.modal.style.right = right;
        this.modal.style.bottom = bottom;
        this.modal.style.left = left;
    };

    btnCloseModalAction(actionBtn) {
        this.closeBtn.addEventListener('click', () => {
            actionBtn.style.display = 'inline';
            this.modal.remove();
        });
    }
    modalAction(actionBtn) {
        actionBtn.style.display = 'none';
        this.modal.addEventListener('click', () => {
            this.modal.style.display = 'flex';
            actionBtn.style.display = 'none';
        }, true)
        document.body.addEventListener('click', (event) => {
            if (event.target === actionBtn) {
                actionBtn.style.display = 'none';
            }
            else {
                this.modal.style.display = 'none';
                // this.modal.remove();
                actionBtn.style.display = 'inline';
            }
        }, true)
    }
    renderModal(actionBtn, modalStyles) {
        this.createModalBlock();
        this.btnCloseModalAction(actionBtn);
        this.addStylesToModal(modalStyles);
        this.modalAction(actionBtn);
    }
}