import { Modal } from "./class_modal.js";
import { Form, FormInput } from "./classes_form.js";
import {renderCardsToBoard } from "./cardbox_create.js";
import {cardToBoard } from "./class_card_to_board.js";
// ---------=== DOM-Elements ===------------------------------------------------------
const login = document.getElementById('btnLogin');
const exit = document.getElementById('btnExit');
export const createVisit = document.getElementById('createVisitBtn');
const notification = document.querySelector('.notification');
const createdCards = document.querySelector('.cards-created');

// ---------------------------------------------------------------------------------------
exit.addEventListener('submit',()=>{alert('Have a nice day')
// login.classList.toggle('display');
// exit.classList.toggle('display');
// createVisit.classList.toggle('display');
});
login.addEventListener('click', () => {
    const modalLogin = new Modal();
    modalLogin.renderModal(login, modalLoginStyles);

    const loginForm = new Form();
    loginForm.renderForm(modalLogin.modal, formLogin);

    const emailField = new FormInput(loginForm.form);
    emailField.renderFormInput(inputEmail);

    const passwordField = new FormInput(loginForm.form);
    passwordField.renderFormInput(inputPassword);

    const enter = new FormInput(loginForm.form);
    enter.renderFormInput(inputEnterBtn);
    
    
    // --------Get info from login-form-------------
    function getLoginInfo() {
        return {
            email: emailField.getValue(),
            password: passwordField.getValue(),
        };
    }

// --------Get Token, Render cards------------------------------------------
    loginForm.form.addEventListener('submit', (e) => {
        e.preventDefault();
        async function getToken(url,notification, createdCards,getCards) {
            await fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(getLoginInfo())
            })
            .then( response => {
                if ( response.status === 401 ) {
                  sessionStorage.removeItem( 'token' );
                  return
                }
                  return (response.text())
                })
                .then(token => {
                    console.log(token);
                  if (typeof token!=='string') {
                    throw new Error('Введен неверный логин или пароль')
                  } else {
                    sessionStorage.setItem('token', token);
                    modalLogin.modal.remove();
                    login.remove();
                    exit.classList.toggle('display');
                    createVisit.classList.toggle('display');
                    renderCardsToBoard(notification, createdCards,getCards);
                  }
                })
              .catch( error => {
                console.error(error)
                  sessionStorage.removeItem('token');
                  window.alert('Server ERROR!')
                })

            }
              
          getToken(getTokenUrl,notification, createdCards,getCards);
        

    })


})






// -------Properties---------------------------------------------------------------------------
const modalLoginStyles = {
    modalClass: "login-modal",
    btnClass: "close-modal-btn",
    top: "5%",
    right: "10%",
    bottom: "70%",
    left: "50%",
};
const formLogin = {
    atributes: {
        id: 'form_login',
        name: 'form-login',
    },
    className: 'cards-create-form',
};
const inputEnterBtn = {
    atributes: {
        type: 'submit',
        id: 'create_form',
        value: 'Войти',
        name: 'submit_btn',
    },
    className: 'form-btn',
};
const inputEmail = {
    atributes: {
        type: 'email',
        required: 'true',
        placeholder: 'Введите e-mail',
        name: 'email',
    },
    className: 'form-input',
};
const inputPassword = {
    atributes: {
        type: 'password',
        required: 'true',
        placeholder: 'Введите пароль',
        name: 'password',
    },
    className: 'form-input',
};

const getTokenUrl ='https://ajax.test-danit.com/api/cards/login';

const getCards = {
    url:'https://ajax.test-danit.com/api/v2/cards',
    styles: {
        formClass:'no',
         inputClassName:'no',
         btnFormClass:'no',
    }
};